#include "menu_window.h"
#include "game/game_window.h"
#include "opencv2/opencv.hpp"

#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MenuWindow w;
    w.show();
    return QApplication::exec();
}