QT       += core gui multimedia opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    camera/camera_manager.cpp \
    camera/camera_widget.cpp \
    game/game_window.cpp \
    game/maze_generation/maze.cpp \
    game/gl_elements/gl_maze.cpp \
    game/gl_elements/gl_camera.cpp \
    game/gl_elements/gl_wall.cpp \
    game/gl_elements/sphere.cpp \
    main.cpp \
    game/top_view.cpp \
    menu_window.cpp \
    options_dialog.cpp

HEADERS += \
    camera/camera_manager.h \
    camera/camera_widget.h \
    game/game_window.h \
    game/maze_generation/maze.h \
    game/maze_generation/cell.h \
    game/gl_elements/gl_maze.h \
    game/gl_elements/gl_camera.h \
    game/gl_elements/gl_wall.h \
    game/gl_elements/sphere.h \
    game/top_view.h \
    menu_window.h \
    options_dialog.h

FORMS += \
    game/game_window.ui \
    menu_window.ui \
    options_dialog.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32 {
    win32-msvc* {
        LIBS     += opengl32.lib glu32.lib
    } else {
        LIBS     += -lopengl32 -lglu32
    }

    OUT_PWD_WIN = $${OUT_PWD}
    OUT_PWD_WIN ~= s,/,\,g
    PWD_WIN = $${PWD}
    PWD_WIN ~= s,/,\,g


    QMAKE_POST_LINK += $$quote(mkdir $${OUT_PWD_WIN}\res) $$escape_expand(\n\t) $$quote(xcopy $${PWD_WIN}\..\res $${OUT_PWD_WIN}\res /E /I)


}
else {
        LIBS     += -lGL -lGLU
}


INCLUDEPATH +=$$(OPENCV_DIR)\..\..\include\


LIBS += -L$$(OPENCV_DIR)\lib \
-lopencv_core420 \
-lopencv_highgui420 \
-lopencv_imgproc420 \
-lopencv_imgcodecs420 \
-lopencv_videoio420 \
-lopencv_features2d420 \
-lopencv_calib3d420 \
-lopencv_objdetect420

