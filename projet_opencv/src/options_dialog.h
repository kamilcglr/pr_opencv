/**
 * @file options_dialog.h
 * @brief This class is associated with a option_dialog.ui to modify the size of the maze.
 */
#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>

namespace Ui {
class OptionsDialog;
}

/**
 * @class This class permit to modify settings. Currently, we can only modify maze size.
 */
class OptionsDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * Create the dialog and set validators.
     */
    explicit OptionsDialog(QWidget *parent = nullptr);

    /**
     * Get entered and validated options by user.
     * @return pair of int : maze_width and maze_height
     */
    std::pair<int, int> getOptions(){return std::pair<int, int>(width_, height_);}

    ~OptionsDialog() override;

private slots:
    /**
     * Convert String value to int and assign to width_.
     * @param arg1
     */
    void on_lineEdit_textEdited(const QString &arg1);

    /**
     * Convert String value to int and assign to height_.
     * @param arg1
     */
    void on_lineEdit_2_textEdited(const QString &arg1);

private:
    Ui::OptionsDialog *ui;
    int width_ = 10;
    int height_ = 6;
};

#endif // OPTIONSDIALOG_H
