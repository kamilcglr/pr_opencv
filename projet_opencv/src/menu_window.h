/**
 * @file menu_window.h
 * @brief This class is associated with a menu_window.ui to main menu of the game.
 */
#ifndef MENUWINDOW_H
#define MENUWINDOW_H

#include <QMainWindow>
#include <game/game_window.h>
#include "options_dialog.h"

namespace Ui {
    class MenuWindow;
}

class MenuWindow : public QMainWindow {
Q_OBJECT

public:
    /**
     * Create the window from .ui. Initiate gif animation and option dialog.
     * @param parent
     */
    explicit MenuWindow(QWidget *parent = nullptr);

    ~MenuWindow() override;

private:
    /**
     * Load a gif from resources as illustration for the game.
     */
    void initiateGIF();

private slots:

    /**
     * Launch the game.
     */
    void on_playButton_clicked();

    /**
     * Show option dialog.
     */
    void on_optionsButton_clicked();

    /**
     * Close window and exit
     */
    void on_quitButton_clicked();

    /**
     * Load option from dialog.
     */
    void getOptions();

private:
    Ui::MenuWindow *ui;
    OptionsDialog *dialog_;
    GameWindow *game_window;
    std::pair<int, int> options = std::pair<int, int>(10, 6); /**< Default option for size of maze */
};

#endif // MENUWINDOW_H
