#include "options_dialog.h"
#include "ui_options_dialog.h"
#include <QValidator>
#include <QString>
#include <iostream>

OptionsDialog::OptionsDialog(QWidget *parent) :
        QDialog(parent),
        ui(new Ui::OptionsDialog) {
    ui->setupUi(this);
    
    ui->lineEdit->setValidator(new QIntValidator(2, 20, ui->lineEdit));
    ui->lineEdit_2->setValidator(new QIntValidator(2, 20, ui->lineEdit_2));

    ui->lineEdit->setMaxLength(2);
    ui->lineEdit_2->setMaxLength(2);
}

OptionsDialog::~OptionsDialog() {
    delete ui;
}

void OptionsDialog::on_lineEdit_textEdited(const QString &arg1) {
    if (this->ui->lineEdit->hasAcceptableInput()) {
        height_ = this->ui->lineEdit->text().toInt();
    } else {
        height_ = 6;
    }
}

void OptionsDialog::on_lineEdit_2_textEdited(const QString &arg1) {
    if (this->ui->lineEdit_2->hasAcceptableInput()) {
        width_ = this->ui->lineEdit_2->text().toInt();
    } else {
        width_ = 10;
    }
}
