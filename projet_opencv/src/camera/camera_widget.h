/**
 * @file camera_widget.h
 * @brief QWidget to display the camera output
 **/
#ifndef PR_OPENCV_CAMERA_WIDGET_H
#define PR_OPENCV_CAMERA_WIDGET_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>

/**
 * @class QWidget that display images from CameraManager.
 */
class CameraWidget : public QWidget {
Q_OBJECT
public:
    /**
     * Set the label.
     * @param parent
     */
    explicit CameraWidget(QWidget *parent = nullptr);

    void resizeEvent(QResizeEvent *) override;

    void setRatio(float ratio);

public slots :

    void setImage(const QImage &new_image);

private:
    QImage image_;
    QLabel *image_label; /**< Label containing the image */

    float grid_ratio;
    QLayout *horizontal_layout_;
};


#endif //PR_OPENCV_CAMERA_WIDGET_H
