#include "camera_manager.h"
#include "opencv2/video/tracking.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <QtGui/QImage>

using namespace cv;
using namespace std;

CameraManager::CameraManager(QObject *parent) : QThread(parent) {

}

void CameraManager::run() {
    this->openCamera();
    forever {
        // Test if end flag is on
        if (terminate_thread_) {
            shutDownCamera();
            break;
        }
        waitKey(50);
        // Get frame2
        cap_ >> frame2_;

        // Mirror effect
        cv::flip(frame2_, frame2_, 1);

        // Extract working rect in frame2 and convert to gray
        cv::cvtColor(Mat(frame2_, working_rect_), frame_rect2_, COLOR_BGR2GRAY);

        // Extract template image in frame1
        Mat templateImage(frame_rect1_, template_rect_);

        // Do the Matching between the working rect in frame2 and the templateImage in frame1
        matchTemplate(frame_rect2_, templateImage, result_image_, TM_CCORR_NORMED);

        // Localize the best match with minMaxLoc
        double minVal;
        double maxVal;
        Point minLoc;
        Point maxLoc;
        minMaxLoc(result_image_, &minVal, &maxVal, &minLoc, &maxLoc);

        // Compute the translation vector between the origin and the matching rect
        Point vect(maxLoc.x - template_rect_.x, maxLoc.y - template_rect_.y);
        //cout << vect.x << "  " << vect.y << endl;
        if (abs(vect.x) > out_of_bounds_ || abs(vect.y) > out_of_bounds_) {
            emit direction("IDLE");
            detectFace();
        } else {
            if (abs(vect.x) > min_detection_ || abs(vect.y) > min_detection_) {
                pause_time_ = 0;
                if (vect.x > min_detection_) {
                    emit direction("RIGHT");
                } else if (vect.x < -min_detection_) {
                    emit direction("LEFT");
                } else if (vect.y > min_detection_) {
                    emit direction("DOWN");
                } else {
                    emit direction("UP");
                }
            } else {
                if (pause_time_ > -1) {
                    pause_time_++;
                    //cout << pauseTime << endl;
                    if (pause_time_ >= 3) {
                        emit direction("IDLE");
                        pause_time_ = -1;
                    }
                }
            }
        }
        // Draw green rectangle and the translation vector
        rectangle(frame2_, working_rect_, Scalar(0, 255, 0), 2);
        Point p(working_center_.x + vect.x, working_center_.y + vect.y);
        arrowedLine(frame2_, working_center_, p, Scalar(255, 255, 255), 2);

        // Send the image through signal to CameraWidget
        // https://stackoverflow.com/questions/11543298/qt-opencv-displaying-images-on-qlabel
        // https://stackoverflow.com/questions/14729417/open-webcamera-with-opencv-and-show-it-with-qlabel-white-window/14729778#14729778
        QImage frame2_image = QImage((uchar *) frame2_.data, frame2_.cols, frame2_.rows, frame2_.step,
                                     QImage::Format_RGB888);
        emit putImage(frame2_image.rgbSwapped());
    }
}

void CameraManager::detectFace() {
    cv::Rect newRect;
    int waitTime = 0;
    while (waitTime < 15 && !terminate_thread_) {
        cap_ >> frame1_;
        flip(frame1_, frame1_, 1);

        // Extract rect1 and convert to gray
        //cout << frame1.size << " " << workingRect.x << " " << workingRect.y << endl;
        cvtColor(frame1_, frame_rect1_, COLOR_BGR2GRAY);
        face_cascade_.detectMultiScale(frame_rect1_, faces_, 1.1, 4, 0, Size(60, 60));

        if (!faces_.empty()) {
            newRect = faces_[0];
            rectangle(frame1_, newRect, Scalar(0, 255, 0), 2);
            QImage frame1_image = QImage((uchar *) frame1_.data, frame1_.cols, frame1_.rows, frame1_.step,
                                         QImage::Format_RGB888);
            emit putImage(frame1_image.rgbSwapped());
            if (abs(working_rect_.x - newRect.x) > 12) {
                waitTime = 0;
            } else {
                waitTime++;
            }
            working_rect_ = newRect;
            template_rect_ = Rect((working_rect_.width - working_rect_.width * 0.2) / 2,
                                (working_rect_.height - working_rect_.height * 0.2) / 2,
                                working_rect_.width * 0.2, working_rect_.height * 0.2);
            working_center_ = Point(working_rect_.x + working_rect_.width / 2, working_rect_.y + working_rect_.height / 2);

            // Create the matchTemplate image result
            int result_cols{};
            int result_rows{};
            result_cols = frame1_.cols - template_width_ + 1;
            result_rows = frame1_.rows - template_height_ + 1;

            /*
             * CV_32FC1 : defines both the depth of each element and the number of channels.
             * 32F stands for a "32bits floating point" and C1 for "single channel"
             * */
            result_image_.create(result_cols, result_rows, CV_32FC1);
            cvtColor(Mat(frame1_, working_rect_), frame_rect1_, COLOR_BGR2GRAY);
            out_of_bounds_ = int((working_rect_.width - template_width_) * 0.35);
            min_detection_ = int((working_rect_.width - template_width_) * 0.1);
            //cout << "width: " << working_rect_.width << " " << template_rect_.width << "  bounds: " << out_of_bounds_ << "  "
            //     << min_detection_ << "  " << endl;
        } else {
            // no faces in camera, send message to user
            emit noFacesOnCamera();
            QImage frame1_image = QImage((uchar *) frame1_.data, frame1_.cols, frame1_.rows, frame1_.step,
                                         QImage::Format_RGB888);
            emit putImage(frame1_image.rgbSwapped());
            CameraManager::msleep(250); // Do not spam main window, we are in a while
        }
    }
}

void CameraManager::openCamera() {
    cap_.open(0); // open the default camera

    cout << "width_ :" << cap_.get(CAP_PROP_FRAME_WIDTH) << endl;
    cout << "height_ :" << cap_.get(CAP_PROP_FRAME_HEIGHT) << endl;
    cap_.set(480, frame_width_);
    cap_.set(CAP_PROP_FRAME_HEIGHT, frame_height_);

    if (!cap_.isOpened())  // check if we succeeded
    {
        cerr << "Error opening the default camera" << endl;
        return;
    }
    if (!face_cascade_.load("res/haarcascade_frontalface_alt.xml")) {
        cerr << "Error loading haarcascade" << endl;
        return;
    }
    detectFace();
}

CameraManager::~CameraManager() {
    mutex_.unlock();
}

void CameraManager::shutDownCamera() {
    cap_.release();
}

void CameraManager::setTerminateThread(bool terminate_thread) {
    terminate_thread_ = terminate_thread;
}
