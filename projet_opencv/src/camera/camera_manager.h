/**
 * @file camera_manager.h
 * @brief QThread object that interact with camera.
 * Created from this example https://doc.qt.io/qt-5/qtcore-threads-mandelbrot-example.html.
 **/
#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

#include "QThread"
#include "QMutex"

#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect.hpp"

#include <cstdio>
#include <iostream>

/**
 * @class Inherited from QThread. This class is connected to the game_window to handle user movements.
 */
class CameraManager : public QThread {
Q_OBJECT
public:
    /**
     * Initializes the default camera and start the detectFace function
     * @param parent
     */
    explicit CameraManager(QObject *parent = nullptr);

    ~CameraManager() override;

    /**
     * Open the camera and load file for classifier
     */
    void openCamera();

    /**
     * This function permit to detect the user's to correctly place the detection zone for the movement
     */
    void detectFace();

    /**
     * Change the flag state
     * @param terminate_thread True when thread should stop
     */
    void setTerminateThread(bool terminate_thread);

signals:

    /**
     * Send the direction of the user's head to move
     * @param dir
     */
    void direction(QString dir);

    /**
     * Send the picture took by the camera to display it on the main window
     */
    void putImage(const QImage &new_image);

    /**
     * Used when the user is not in front of the camera and there is no detection
     */
    void noFacesOnCamera();

public slots:

    void shutDownCamera();

protected:
    void run() override;

private:

    /**
     * Thread related content
     */
    QMutex mutex_;

    bool terminate_thread_ = false; /**< Flag to indicate that the thread should stop when true */

private:

    /**
     * Parameters of the detection zone and size of the picture taken by the camera
     */
    int frame_width_ = 480;
    int frame_height_ = 480;
    int template_width_ = 50;
    int template_height_ = 50;

    /**
     * Parameters to detect a movement or to recalibrate the camera if necessary
     */
    int min_detection_ = -25;
    int out_of_bounds_ = 120;

    int pause_time_ = 0; /**< Measure the time before sending idle */

    /**
     * Elements created on the picture to detect movement between 2 pictures
     */
    cv::Rect working_rect_;
    cv::Rect template_rect_;
    cv::Point working_center_;

    cv::Mat frame1_, frame2_, frame_rect1_, frame_rect2_;

    cv::VideoCapture cap_; /**< Object that controls the webcam */

    cv::Mat result_image_;

    cv::CascadeClassifier face_cascade_;
    std::vector<cv::Rect> faces_; /**< Vector containing detected faces */
};

#endif // CAMERAMANAGER_H
