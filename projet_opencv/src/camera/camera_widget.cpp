#include <QtGui/QPainter>
#include <QColorSpace>
#include <QImageReader>
#include <QImageWriter>
#include <QLabel>
#include <QtWidgets/QHBoxLayout>
#include <iostream>
#include "camera_widget.h"

CameraWidget::CameraWidget(QWidget *parent) : QWidget(parent), image_label(new QLabel) {
    grid_ratio = parent->height()/5.0;

    image_label->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    image_label->setScaledContents(true);
    horizontal_layout_ = new QHBoxLayout;
    horizontal_layout_->addWidget(image_label);

    this->setLayout(horizontal_layout_);
}

void CameraWidget::setImage(const QImage &new_image) {
    image_ = new_image;
    image_label->setPixmap(QPixmap::fromImage(image_).scaledToHeight(grid_ratio, Qt::FastTransformation));
}

void CameraWidget::setRatio(float ratio) {
    this->grid_ratio = ratio;
}

void CameraWidget::resizeEvent(QResizeEvent *) {
}
