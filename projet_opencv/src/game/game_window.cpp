#include "game_window.h"
#include "ui_game_window.h"
#include "QPoint"
#include "top_view.h"
#include <iostream>
#include <opencv2/highgui.hpp>
#include <QtGui/QFontDatabase>
#include <QtCore/QPropertyAnimation>
#include <QtWidgets/QGraphicsOpacityEffect>
#include <QtWidgets/QMessageBox>
#include <QtCore/QDateTime>
#include <QtMultimedia/QMediaPlayer>
#include <QtMultimedia/QMediaPlaylist>
#include <QtCore/QDir>

GameWindow::GameWindow(int maze_width, int maze_height, QWidget *parent)
        : QMainWindow(parent), ui(new Ui::GameWindow) {
    QFontDatabase::addApplicationFont(":/fonts/Roboto-Regular.ttf");

    ui->setupUi(this);

    initStatusBar();
    initMusic();

    ui->gl_maze->initializeMaze(maze_width, maze_height);

    // enable or disable mouse tracking
    ui->gl_maze->changeMouseTacking(mouse_movements_);

    // Link generated maze to top view maze
    ui->top_view->setMaze(ui->gl_maze->getMaze());

    // Launch timer to update chrono
    update_chrono_.start(1000);
    elapsed_timer_from_launch_.start();

    // Start the thread
    camera_manager_.start();

    QWidget::connect(&update_chrono_, &QTimer::timeout, [&] {
        time_label_->setText(elapsedTimeSinceLaunchFormatted());
    });

    // Connect timer to screen refresh
    QWidget::connect(ui->gl_maze, &GLMaze::positionChange,
                     ui->top_view, &TopView::setNewPositionAndTarget);

    // Connect user position change to top view
    QWidget::connect(ui->gl_maze, &GLMaze::positionChange,
                     ui->top_view, &TopView::setNewPositionAndTarget);

    // End of game detection to message to the user
    QWidget::connect(ui->gl_maze, &GLMaze::endOfGame,
                     this, &GameWindow::onEndOfGame);

    // Connect gl_maze messages to statusbar
    QWidget::connect(ui->gl_maze, &GLMaze::sendMessage, this, &GameWindow::onGameMessage);

    QWidget::connect(ui->gl_maze, &GLMaze::nextMusic, [&] {
        playlist_->next();
    });

    // Direction detected by camera to main window
    QWidget::connect(&camera_manager_, &CameraManager::direction, this, &GameWindow::directionMove);

    // Image from camera to widget on screen
    QWidget::connect(&camera_manager_, &CameraManager::putImage, ui->camera_widget, &CameraWidget::setImage);

    // Link face movements with camera movements
    QWidget::connect(&camera_manager_, &CameraManager::direction, ui->gl_maze->getGlCamera(),
                     &GLCamera::changeDirection);

    // When camera manager can not detect faces on camera, send message to status bar
    QWidget::connect(&camera_manager_, &CameraManager::noFacesOnCamera, [&] {
        direction_label_->setText("Aucun visage détecté.");
        direction_label_->setStyleSheet("color: rgb(207,102,121);");
    });

    // Connect top_view_timer_ with top_view to show it
    QWidget::connect(&top_view_timer_, &QTimer::timeout, [&] {
        this->ui->top_view->setVisible(true);
    });
}

GameWindow::~GameWindow() {
    delete ui;
    delete time_label_;
    delete direction_label_;
    delete game_label_;
    delete music_;
    delete playlist_;
}

void GameWindow::directionMove(const QString &dir) {
    if (dir == "IDLE") {
        // launch timer before showing
        top_view_timer_.start(2000);
    } else {
        top_view_timer_.stop();
        this->ui->top_view->setVisible(false);
    }
    direction_label_->setText(dir);
    direction_label_->setStyleSheet("color: white;");
}

void GameWindow::resizeEvent(QResizeEvent *) {
    ui->camera_widget->setRatio(this->height() / 5.0);
}

void GameWindow::keyPressEvent(QKeyEvent *event) {
    switch (event->key()) {
        case Qt::Key_Escape: {
            // close this window
            emit onEndOfGame(false);
        }
        default: {
            ui->gl_maze->keyPressEvent(event);
        }
    }
}

void GameWindow::keyReleaseEvent(QKeyEvent *event) {
    ui->gl_maze->keyReleaseEvent(event);
}

void GameWindow::onEndOfGame(bool exit_found) {
    playlist_->next();
    // Stop movements
    ui->gl_maze->getTimer().stop();

    this->update_chrono_.stop();

    // Stop camera manager
    this->camera_manager_.setTerminateThread(true);
    if (!this->camera_manager_.wait(5000)) //Wait until it actually has terminated (max. 5 sec)
    {
        qWarning("Thread deadlock detected, bad things may happen !!!");
        this->camera_manager_.terminate(); //Thread didn't exit in time, probably deadlocked, terminate it!
        this->camera_manager_.wait(); //Note: We have to wait again here!
    }

    if (exit_found) {
        // Show end of game Message
        QMessageBox end_message_box;

        end_message_box.setIconPixmap(QPixmap("res/pictures/firework.jpg").scaled(244, 326));
        end_message_box.setWindowTitle("Bravo !");
        end_message_box.setText("Vous êtes sortis du labyrinthe avec un " + elapsedTimeSinceLaunchFormatted());
        end_message_box.setStyleSheet("color: white;"
                                      "background-color: rgb(18,18,18);"
                                      "font: bold 17pt 'Roboto';"
                                      "font-style: normal;");

        QAbstractButton *go_to_main = reinterpret_cast<QAbstractButton *>(end_message_box.addButton(
                tr("Retourner au Menu Principal"), QMessageBox::ActionRole));
        end_message_box.exec();

        if (end_message_box.clickedButton() == go_to_main) {
            // Close window and enable main_window
            emit enablePlayButton();
            music_->stop();
            this->close();
        }
    } else {
        // Close window and enable main_window
        emit enablePlayButton();
        music_->stop();
        this->close();
    }
}

QString GameWindow::elapsedTimeSinceLaunchFormatted() {
    int seconds = ((int) elapsed_timer_from_launch_.elapsed() / 1000) % 60;
    int minutes = ((int) elapsed_timer_from_launch_.elapsed() / 60000) % 60;
    int hours = ((int) elapsed_timer_from_launch_.elapsed() / 3600000) % 24;
    return QString(" Temps écoulé : ") + QTime(hours, minutes, seconds).toString("hh:mm:ss");
}

void GameWindow::initStatusBar() {
    auto *widget = new QWidget();
    auto *layout = new QGridLayout(widget);

    time_label_ = new QLabel();
    direction_label_ = new QLabel();

    game_label_ = new QLabel("Trouvez la sphère pour débloquer la sortie");
    game_label_->setStyleSheet("color:rgb(3, 218, 198);");

    layout->addWidget(direction_label_, 0, 0, 1, 1, Qt::AlignVCenter | Qt::AlignLeft);
    layout->addWidget(game_label_, 0, 1, 1, 1, Qt::AlignVCenter | Qt::AlignCenter);
    layout->addWidget(time_label_, 0, 2, 1, 1, Qt::AlignVCenter | Qt::AlignRight);
    ui->statusbar->addWidget(widget, 1);
}

void GameWindow::onGameMessage(const QString &message) {
    game_label_->setText(message);
}

void GameWindow::initMusic() {
    playlist_ = new QMediaPlaylist();
    std::cout << QDir::current().path().toStdString() << std::endl;
    playlist_->addMedia(QUrl::fromLocalFile(QDir::current().path() + "/res/sounds/1.mp3"));
    playlist_->addMedia(QUrl::fromLocalFile(QDir::current().path() + "/res/sounds/2.mp3"));
    playlist_->addMedia(QUrl::fromLocalFile(QDir::current().path() + "/res/sounds/3.mp3"));
    playlist_->setPlaybackMode(QMediaPlaylist::Loop);

    music_ = new QMediaPlayer();
    music_->setPlaylist(playlist_);
    music_->play();
}
