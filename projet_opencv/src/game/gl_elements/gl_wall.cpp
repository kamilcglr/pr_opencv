#include <cmath>
#include <GL/gl.h>
#include <iostream>
#include "gl_wall.h"

GLWall::GLWall(float position_x1, float position_z1, bool vertical,
               float thickness, bool is_post, GLuint *texture) : position_x1_(position_x1),
                                                                 position_z1_(position_z1),
                                                                 vertical_(vertical),
                                                                 thickness_(thickness),
                                                                 texture(texture) {
    if (is_post) { // draw same length faces
        length_ = thickness_ * 2;
        texture_bl_ = std::pair<float, float>(0, 0);
        texture_tl_ = std::pair<float, float>(0, 1);
        texture_tr_ = std::pair<float, float>(0.1, 1);
        texture_br_ = std::pair<float, float>(0.1, 0);
    } else {
        length_ = 1.5; // length - thickness of a wall ( thickness * 2)
        texture_bl_ = std::pair<float, float>(0, 0);
        texture_tl_ = std::pair<float, float>(0, 1);
        texture_tr_ = std::pair<float, float>(0.3, 1);
        texture_br_ = std::pair<float, float>(0.3, 0);
    }

    elevation_ = 2;

    this->ambient_diffuse_color_ = new GLfloat[4]{0.8, 0.8, 0.8, 1.0};
    this->specular_color_ = new GLfloat[4]{0.8, 0.8, 0.8, 1.0};
    this->emitting_color_ = new GLfloat[4]{0, 0, 0, 0};

    /*
     * Left and Right are the longer faces
     * Front and Behind are the shortest faces
     */
    if (!vertical) {
        if (!is_post) {
            position_x1_ += thickness_;
        } else {
            position_x1_ -= thickness_;
        }

        //Bottom
        bottom_1_ = QVector3D(0, 0, +thickness_);
        bottom_2_ = QVector3D(0, 0, -thickness_);
        bottom_3_ = QVector3D(length_, 0, -thickness_);
        bottom_4_ = QVector3D(length_, 0, thickness_);

        //Top
        top_1_ = QVector3D(0, elevation_, +thickness_);
        top_2_ = QVector3D(0, elevation_, -thickness_);
        top_3_ = QVector3D(length_, elevation_, -thickness_);
        top_4_ = QVector3D(length_, elevation_, thickness_);

        //Left
        left_1_ = QVector3D(0, 0, -thickness_);
        left_2_ = QVector3D(0, elevation_, -thickness_);
        left_3_ = QVector3D(length_, elevation_, -thickness_);
        left_4_ = QVector3D(length_, 0, -thickness_);

        //Right
        right_1_ = QVector3D(0, 0, +thickness_);
        right_2_ = QVector3D(0, elevation_, +thickness_);
        right_3_ = QVector3D(length_, elevation_, +thickness_);
        right_4_ = QVector3D(length_, 0, +thickness_);

        //Front
        front_1_ = QVector3D(0, 0, -thickness_);
        front_2_ = QVector3D(0, elevation_, -thickness_);
        front_3_ = QVector3D(0, elevation_, +thickness_);
        front_4_ = QVector3D(0, 0, +thickness_);

        //Behind
        back_1_ = QVector3D(+length_, 0, -thickness_);
        back_2_ = QVector3D(+length_, elevation_, -thickness_);
        back_3_ = QVector3D(+length_, elevation_, +thickness_);
        back_4_ = QVector3D(+length_, 0, +thickness_);
    } else {
        position_z1_ += thickness_;

        //Bottom
        bottom_1_ = QVector3D(-thickness_, 0, 0);
        bottom_2_ = QVector3D(+thickness_, 0, 0);
        bottom_3_ = QVector3D(+thickness_, 0, length_);
        bottom_4_ = QVector3D(-thickness_, 0, length_);

        //Top
        top_1_ = QVector3D(-thickness_, elevation_, 0);
        top_2_ = QVector3D(+thickness_, elevation_, 0);
        top_3_ = QVector3D(+thickness_, elevation_, length_);
        top_4_ = QVector3D(-thickness_, elevation_, length_);

        //Left
        left_1_ = QVector3D(-thickness_, 0, 0);
        left_2_ = QVector3D(-thickness_, elevation_, 0);
        left_3_ = QVector3D(-thickness_, elevation_, length_);
        left_4_ = QVector3D(-thickness_, 0, length_);

        //Right
        right_1_ = QVector3D(+thickness_, 0, 0);
        right_2_ = QVector3D(+thickness_, elevation_, 0);
        right_3_ = QVector3D(+thickness_, elevation_, length_);
        right_4_ = QVector3D(+thickness_, 0, length_);

        //Front
        front_1_ = QVector3D(-thickness_, 0, length_);
        front_2_ = QVector3D(-thickness_, elevation_, length_);
        front_3_ = QVector3D(+thickness_, elevation_, length_);
        front_4_ = QVector3D(+thickness_, 0, length_);

        //Behind
        back_1_ = QVector3D(-thickness_, 0, 0);
        back_2_ = QVector3D(-thickness_, elevation_, 0);
        back_3_ = QVector3D(+thickness_, elevation_, 0);
        back_4_ = QVector3D(+thickness_, 0, 0);
    }

}

void GLWall::display() const {
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, ambient_diffuse_color_);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular_color_);
    glMaterialfv(GL_FRONT, GL_EMISSION, emitting_color_);
    glMaterialf(GL_FRONT, GL_SHININESS, 100);

    glPushMatrix();
    glTranslated(position_x1_, 0, position_z1_);

    if (!vertical_) {
        //Bottom
        glBegin(GL_QUADS);
        glNormal3f(0.0, -1.0, 0.0);
        glVertex3f(bottom_1_.x(), bottom_1_.y(), bottom_1_.z());
        glVertex3f(bottom_2_.x(), bottom_2_.y(), bottom_2_.z());
        glVertex3f(bottom_3_.x(), bottom_3_.y(), bottom_3_.z());
        glVertex3f(bottom_4_.x(), bottom_4_.y(), bottom_4_.z());
        glEnd();

        //Top
        glBegin(GL_QUADS);
        glNormal3f(0.0, 1.0, 0.0);
        glVertex3f(top_1_.x(), top_1_.y(), top_1_.z());
        glVertex3f(top_2_.x(), top_2_.y(), top_2_.z());
        glVertex3f(top_3_.x(), top_3_.y(), top_3_.z());
        glVertex3f(top_4_.x(), top_4_.y(), top_4_.z());
        glEnd();

        //Left
        glBindTexture(GL_TEXTURE_2D, this->texture[0]);
        glBegin(GL_QUADS);
        glNormal3f(0.0, 0.0, -1.0);
        glTexCoord2f(texture_bl_.first, texture_bl_.second);
        glVertex3f(left_1_.x(), left_1_.y(), left_1_.z());
        glTexCoord2f(texture_tl_.first, texture_tl_.second);
        glVertex3f(left_2_.x(), left_2_.y(), left_2_.z());
        glTexCoord2f(texture_tr_.first, texture_tr_.second);
        glVertex3f(left_3_.x(), left_3_.y(), left_3_.z());
        glTexCoord2f(texture_br_.first, texture_br_.second);
        glVertex3f(left_4_.x(), left_4_.y(), left_4_.z());
        glEnd();

        //Right
        glBindTexture(GL_TEXTURE_2D, this->texture[0]);
        glBegin(GL_QUADS);
        glNormal3f(0.0, 0.0, +1.0);
        glTexCoord2f(texture_bl_.first, texture_bl_.second);
        glVertex3f(right_1_.x(), right_1_.y(), right_1_.z());
        glTexCoord2f(texture_tl_.first, texture_tl_.second);
        glVertex3f(right_2_.x(), right_2_.y(), right_2_.z());
        glTexCoord2f(texture_tr_.first, texture_tr_.second);
        glVertex3f(right_3_.x(), right_3_.y(), right_3_.z());
        glTexCoord2f(texture_br_.first, texture_br_.second);
        glVertex3f(right_4_.x(), right_4_.y(), right_4_.z());
        glEnd();

        //Front
        glBindTexture(GL_TEXTURE_2D, this->texture[0]);
        glBegin(GL_QUADS);
        glNormal3f(-1.0, 0.0, 0);
        glTexCoord2f(0, 0);
        glVertex3f(front_1_.x(), front_1_.y(), front_1_.z());
        glTexCoord2f(0, 1);
        glVertex3f(front_2_.x(), front_2_.y(), front_2_.z());
        glTexCoord2f(0.1, 1);
        glVertex3f(front_3_.x(), front_3_.y(), front_3_.z());
        glTexCoord2f(0.1, 0);
        glVertex3f(front_4_.x(), front_4_.y(), front_4_.z());
        glEnd();

        //Behind
        glBindTexture(GL_TEXTURE_2D, this->texture[0]);
        glBegin(GL_QUADS);
        glNormal3f(1.0, 0.0, 0.0);
        glTexCoord2f(0, 0);
        glVertex3f(back_1_.x(), back_1_.y(), back_1_.z());
        glTexCoord2f(0, 1);
        glVertex3f(back_2_.x(), back_2_.y(), back_2_.z());
        glTexCoord2f(0.1, 1);
        glVertex3f(back_3_.x(), back_3_.y(), back_3_.z());
        glTexCoord2f(0.1, 0);
        glVertex3f(back_4_.x(), back_4_.y(), back_4_.z());
        glEnd();
    } else if (vertical_) {
        //Bottom
        glBegin(GL_QUADS);
        glNormal3f(0.0, -1.0, 0.0);
        glVertex3f(bottom_1_.x(), bottom_1_.y(), bottom_1_.z());
        glVertex3f(bottom_2_.x(), bottom_2_.y(), bottom_2_.z());
        glVertex3f(bottom_3_.x(), bottom_3_.y(), bottom_3_.z());
        glVertex3f(bottom_4_.x(), bottom_4_.y(), bottom_4_.z());
        glEnd();

        //Top
        glBegin(GL_QUADS);
        glNormal3f(0.0, 1.0, 0.0);
        glVertex3f(top_1_.x(), top_1_.y(), top_1_.z());
        glVertex3f(top_2_.x(), top_2_.y(), top_2_.z());
        glVertex3f(top_3_.x(), top_3_.y(), top_3_.z());
        glVertex3f(top_4_.x(), top_4_.y(), top_4_.z());
        glEnd();

        //Left
        glBindTexture(GL_TEXTURE_2D, this->texture[0]);
        glBegin(GL_QUADS);
        glNormal3f(-1.0, 0.0, 0);
        glTexCoord2f(texture_bl_.first, texture_bl_.second);
        glVertex3f(left_1_.x(), left_1_.y(), left_1_.z());
        glTexCoord2f(texture_tl_.first, texture_tl_.second);
        glVertex3f(left_2_.x(), left_2_.y(), left_2_.z());
        glTexCoord2f(texture_tr_.first, texture_tr_.second);
        glVertex3f(left_3_.x(), left_3_.y(), left_3_.z());
        glTexCoord2f(texture_br_.first, texture_br_.second);
        glVertex3f(left_4_.x(), left_4_.y(), left_4_.z());
        glEnd();

        //Right
        glBindTexture(GL_TEXTURE_2D, this->texture[0]);
        glBegin(GL_QUADS);
        glNormal3f(1.0, 0.0, 0.0);
        glTexCoord2f(texture_bl_.first, texture_bl_.second);
        glVertex3f(right_1_.x(), right_1_.y(), right_1_.z());
        glTexCoord2f(texture_tl_.first, texture_tl_.second);
        glVertex3f(right_2_.x(), right_2_.y(), right_2_.z());
        glTexCoord2f(texture_tr_.first, texture_tr_.second);
        glVertex3f(right_3_.x(), right_3_.y(), right_3_.z());
        glTexCoord2f(texture_br_.first, texture_br_.second);
        glVertex3f(right_4_.x(), right_4_.y(), right_4_.z());
        glEnd();

        //Front
        glBindTexture(GL_TEXTURE_2D, this->texture[0]);
        glBegin(GL_QUADS);
        glNormal3f(0.0, 0.0, 1.0);
        glTexCoord2f(texture_bl_.first, texture_bl_.second);
        glVertex3f(front_1_.x(), front_1_.y(), front_1_.z());
        glTexCoord2f(texture_tl_.first, texture_tl_.second);
        glVertex3f(front_2_.x(), front_2_.y(), front_2_.z());
        glTexCoord2f(texture_tr_.first, texture_tr_.second);
        glVertex3f(front_3_.x(), front_3_.y(), front_3_.z());
        glTexCoord2f(texture_br_.first, texture_br_.second);
        glVertex3f(front_4_.x(), front_4_.y(), front_4_.z());
        glEnd();

        //Behind
        glBindTexture(GL_TEXTURE_2D, this->texture[0]);
        glBegin(GL_QUADS);
        glNormal3f(0.0, 0.0, -1.0);
        glTexCoord2f(0, 0);
        glVertex3f(back_1_.x(), back_1_.y(), back_1_.z());
        glTexCoord2f(0, 1);
        glVertex3f(back_2_.x(), back_2_.y(), back_2_.z());
        glTexCoord2f(0.1, 1);
        glVertex3f(back_3_.x(), back_3_.y(), back_3_.z());
        glTexCoord2f(0.1, 0);
        glVertex3f(back_4_.x(), back_4_.y(), back_4_.z());
        glEnd();
    }
    glPopMatrix();
}

GLWall::~GLWall() {
    this->texture = nullptr;
    delete[]ambient_diffuse_color_;
    delete[]specular_color_;
    delete[]emitting_color_;
}
