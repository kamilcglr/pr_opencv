/**
 * @file gl_camera.h
 * @brief This class represent the camera object in openGL.
 * Created with the help of this tutorial : https://openclassrooms.com/fr/courses/167717-creez-des-programmes-en-3d-avec-opengl/167148-controle-avance-de-la-camera-partie-2-2
 */
#ifndef GLCAMERA_H
#define GLCAMERA_H

#include <map>
#include <string>
#include <QtCore/Qt>
#include <QtCore/QEvent>
#include <QtGui/QMouseEvent>
#include <QMainWindow>
#include <QtGui/QVector3D>

/**
 * @class Permit to look at scene but also to move. Represents the user.
 * This class also handle keyboard movements.
 */
class GLCamera : public QObject {
Q_OBJECT
    /**< Mapping matrix between actions and their state */
    typedef std::map<Qt::Key, bool> KeyStates;

    /**< Mapping between actions and keys */
    typedef std::map<std::string, Qt::Key> KeyConf;

public:
    /**
     * Constructor, initialize fields.
     * @param position by default
     */
    explicit GLCamera(const QVector3D &position = QVector3D(0, 0, 1));

    /**
     * Verify each coordinate of new position independently and validate them
     * @param calculated_new_position the new position to verify
     * @param old_position if a coordinate is not valid, change it to old value
     * @return new valid position
     */
    QVector3D verifyNewPosition(const QVector3D &old_position, const QVector3D &calculated_new_position);

    /**
     * Check if the x coordinate is valid by verifying that it is in the maze limits. Then verify that it is not in
     * posts then verify wall. The test is done only if the position is near to a wall (margin).
     * @param position
     * @return bool if true, the x position is OK
     */
    bool positionXisValid(const QVector3D &position);

    /**
     * Check if the z coordinate is valid by verifying that it is in the maze limits. Then verify that it is not in
     * posts then verify wall. The test is done only if the position is near to a wall (margin).
     * @param position
     * @return bool if true, the z position is OK
     */
    bool positionZisValid(const QVector3D &position);

    /**
    * Handle mouse motion to control direction of camera.
    * @param event with the position of the mouse
    */
    virtual void OnMouseMotion(const QMouseEvent &event);

    /**
    * Handle Key presses and releases. Evaluate each configured keys.
    * @param event with the states of pressed keys
    */
    virtual void OnKeyboard(const QKeyEvent &event);

    /**
     * Calculate the new position and target of camera.
     * @param time_step time elapsed to calculate new position.
     * @warning this function could break position validator if the speed is too high.
     */
    virtual void animate(qint64 time_step);

    /**
     * Sets the position of camera and change target according to it.
     * @param position
     */
    virtual void setPosition(const QVector3D &position);

    /**
     * Place the camera in scene. If god_view the camera is at the top.
     */
    virtual void look();

    /**
     * Each time the angles are changed, the camera orientation vector, forward_, and the lateral vector, left_,
     * must be recalculated. The forward_ vector defines both where to look, target_, and in which direction to move forward.
     * The left_ vector is used for lateral movement.
     */
    void VectorsFromAngles();

    /**
    * Association of walls vectors to know the positions of the walls.
    * @param walls
    */
    void set_walls(std::vector<std::vector<bool>> walls);

    ~GLCamera() override;

    [[nodiscard]] const QVector3D &get_position() const;

    [[nodiscard]] const QVector3D &get_target() const;

public slots:

    /**
     * This slots is connected to CameraManager. The OpenCV Camera sends the direction to the GLCamera. According to the
     * direction, the state of the key is changed
     * @param direction "LEFT", "RIGHT", "TOP", "BOTTOM", "IDLE"
     */
    void changeDirection(const QString &direction);

private:
    // Camera
    QVector3D position_; /**< Absolute camera position */
    QVector3D target_; /**< Absolute looked point with the camera */
    QVector3D forward_; /**< Vector giving the direction of the look (and thus of the forward movement) */
    QVector3D left_; /**< Eye-perpendicular vector for lateral displacement */
    float phi_; /**< Vertical rotation angle of the camera */
    float theta_;/**< angle of horizontal rotation of the camera (around the vertical) */

    float speed_; /**< Camera travel speed @warning do not put too high value because it could break position validator */
    float sensitivity_; /** Camera sensitivity to mouse movements and faces movements */

    bool god_view_ = false; /**< When true, give a top view of maze. Enabled with G key */
    std::vector<std::vector<bool>> walls_; /**< 2D matrix of walls. If there is a wall between 0 0 and 0 1
                                            *  then walls_[0][0] and walls_[0][1] is true */

    KeyConf keyconf_ = {
            {"reset",        Qt::Key_Space},
            {"god",          Qt::Key_G},
            {"left",         Qt::Key_A},
            {"right",        Qt::Key_E},

            {"forward",      Qt::Key_Z},
            {"backward",     Qt::Key_S},
            {"strafe_left",  Qt::Key_Q},
            {"strafe_right", Qt::Key_D},
            {"boost",        Qt::Key_Shift}
    };/**< Mapping between movements and Qt Keys */

    KeyStates keystates_ = {
            {keyconf_["reset"],        false},
            {keyconf_["god"],          false},
            {keyconf_["left"],         false},
            {keyconf_["right"],        false},
            {keyconf_["forward"],      false},
            {keyconf_["backward"],     false},
            {keyconf_["strafe_left"],  false},
            {keyconf_["strafe_right"], false},
            {keyconf_["boost"],        false}
    }; /**< Mapping between movements and states */

    // Positions of mouse last time there were checked
    int old_x_;
    int old_y_;
};

#endif