/**
 * @file gl_maze.h
 * @brief QGLWidget implementation containing composed with openGL elements (walls, spheres, light...) and maze.
 */
#ifndef PR_OPENCV_GL_MAZE_H
#define PR_OPENCV_GL_MAZE_H

#include <QGLWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QTimer>
#include <QtGui/QVector3D>
#include <QtCore/QElapsedTimer>
#include <GL/glu.h>
#include "game/maze_generation/maze.h"
#include "gl_camera.h"
#include "gl_wall.h"
#include "sphere.h"

#define FPS 60

/**
 * @class Main class of the game. Create the maze and other openGl objects.
 */
class GLMaze : public QGLWidget {
Q_OBJECT

public:
    /**
     * This function launches the game.
     * Create maze object. Generate walls_ with the position of each wall. Start the timer to update scene.
     * @param size_x int the length
     * @param size_z int the height
     */
    void initializeMaze(int size_x, int size_z);

    /**
     * Initialize the path format
     * @param parent
     */
    explicit GLMaze(QWidget *parent = nullptr);

    /**
     * Enable or disable mouseTracking to change camera orientation.
     * @param state if true enable mouseTracking, if false disable it.
     */
    void changeMouseTacking(bool state);

    /**
     * Get the pointer to the maze object as const
     * @return Pointer to maze_ object
     */
    [[nodiscard]] Maze *getMaze() const;

    /**
     * Get the pointer to the camera object
     * @return Pointer to gl_camera_
     */
    [[nodiscard]] GLCamera *getGlCamera() const;


    /*******************************************************************************************************************
    *                                                    Events                                                        *
    *******************************************************************************************************************/

    /**
     * When the user press a Key, transmit the event to gl_camera_
     */
    void keyPressEvent(QKeyEvent *event) override;

    /**
    * When the user release a Key, transmit the event to gl_camera_
    */
    void keyReleaseEvent(QKeyEvent *event) override;

    /**
    * When the user move the mouse, transmit the event to gl_camera_
    */
    void mouseMoveEvent(QMouseEvent *event) override;

    /**
    * @return QTimer reference
    */
    QTimer &getTimer();

signals:

    /**
     * Send the new position and the new target of the gl_camera_
     * @param new_position
     * @param new_target
     */
    void positionChange(QPointF new_position, QPointF new_target);

    /**
     * Send the message that game has terminated, because either the exit is found or the user exits the game
     * @param exit_found true if exit is found, false otherwise
     */
    void endOfGame(bool exit_found);

    /**
     * Send a message to main_window
     * @param msg this message will be displayed to user in statusbar
     */
    void sendMessage(QString msg);

    /**
     * Ask main_window to pass to the next music.
     */
    void nextMusic();

public slots:

    /**
     * Generate exit and rebuilt wall vectors.
     */
    void showExit();

private:
    /**
     * This function is called on .show. Be aware to create all elements before.
     */
    void initializeGL() override;

    /**
     * Called by openGL when the window is resized. Modify the perspective and the viewport to the new size.
     * @param width of the widget
     * @param height of the widget
     */
    void resizeGL(int width, int height) override;

    /**
     * Draw all visual elements in scene. Please put the camera first, then other objects.
     * @warning Please, do not change order of drawing, or if it is necessary verify lights, textures and colors
     * resulting from previous drawing.
     */
    void paintGL() override;

    /**
     * Load the textures and create GLWall objects. Create the light and sphere.
     */
    void createGLElement();

    /**
     * Draw all walls in gl_walls_.
     */
    void displayWalls();

    /**
     * DEBUG
     * Draw a 2D grid on the floor.
     */
    static void drawGrid();

    /**
     * Draw a 200 * 200 textured floor
     */
    void drawFloor();

    /**
     * Draw a 400*400 sky. Use of special colors to make a sunset effect.
     */
    void drawSky();

    /**
    * Initialize the 2D walls_ vector that contains info about the presence of a wall.
     * If walls_[0][1] == true and walls_[1][1] == true, then, there is a wall between z = 0 and z = 1 at abscissa x = 1.
    */
    void createWallsVectors();

    /**
     * Create GLWall objects. There are walls and posts. The size of the maze is enlarged with a factor_scale_ (x2)
     * ratio for a better experience.
     */
    void createGLWalls();

    /**
     * DEBUG
     * Display the walls_ matrix in console.
     */
    void displayWallsVectors();

    /**
     * Load and generate all textures.
     */
    void loadTextures();

    /**
     * Check the position of user (gl_camera_) to test if it is on sphere_ or on exit.
     */
    void gameStatus();

private:
    Maze *maze_;  /**< Generated maze with Prim's algorithm. */
    std::vector<std::vector<bool>> walls_; /**<  2D matrix that define if there is a wall at a x, z position */
    int factor_scale_ = 2; /**< For a better experience, gl_maze size = maze size * factor_scale_ */

    // For animation
    QElapsedTimer elapsed_timer_; /**< Measures the amount of time between each scene refresh */
    qint64 time_elapsed_; /**< Value of the elapsed time between each refresh */
    QElapsedTimer elapsed_timer_since_launch; /**< Measures the elapsed time since launch */

    // Timer to update scene
    QTimer timer_; /**< Timer to update the scene */
    const qint64 interval_ = 1000 / FPS; /**< Interval of refresh, calculated with FPS */

    // Visual Elements
    GLCamera *gl_camera_; /**< Point of view and user position */
    std::list<GLWall> gl_walls_;
    Sphere *sphere_; /**< Telecoms Sphere */
    Sphere *moon_; /**< Moon Sphere */

    bool sphere_found_ = false; /**< True */
    std::pair<float, float> exit_position_;

    GLuint *textures_ = new GLuint[4]; /**< Array of pointer to the loaded textures */

    GLfloat sky_ambient_[4] = {45 / 255.0, 85 / 255.0, 180 / 255.0, 1.0}; /**< Dark blue */
    GLfloat sky_diffuse_[4] = {254 / 255.0, 192 / 255.0, 81 / 255.0, 1.0}; /**< Orange */
    GLfloat sky_specular_[4] = {1, 1, 1, 1};

    GLfloat floor_ambient_diffuse_[4] = {0.8, 0.8, 0.8, 1.0};
    GLfloat floor_specular_[4] = {0.8, 0.8, 0.8, 1.0};

    GLfloat *light1_; /**< Position of the second light on the sky. This light gives a sunset effect*/
    GLfloat light1_ambient_[4] = {1, 1, 1, 1};
    GLfloat light1_diffuse_[4] = {254 / 255.0, 192 / 255.0, 81 / 255.0, 1.0}; /**< Orange */
    GLfloat light1_specular_[4] = {255 / 255.0, 229 / 255.0, 119 / 255.0}; /**< Yellow */

    bool mouse_events_ = false; /**< if false, mouse movements will be rejected */
};


#endif //PR_OPENCV_GL_MAZE_H


