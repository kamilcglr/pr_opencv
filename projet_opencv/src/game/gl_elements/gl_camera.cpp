#define _USE_MATH_DEFINES // for C++ M_PI

#include "gl_camera.h"
#include <cmath>
#include <GL/glu.h>
#include <QtCore/Qt>
#include <QtCore/QEvent>
#include <QtGui/QMouseEvent>
#include <utility>

GLCamera::GLCamera(const QVector3D &position) : QObject() {
    position_ = position;
    phi_ = 0;
    theta_ = 0;
    old_x_ = QCursor::pos().x();
    old_y_ = QCursor::pos().y();

    VectorsFromAngles();

    speed_ = 0.001;
    sensitivity_ = 0.1;
}

void GLCamera::OnMouseMotion(const QMouseEvent &event) {
    int motionX = event.x() - old_x_;
    int motionY = event.y() - old_y_;

    theta_ -= motionX * sensitivity_;
    phi_ -= motionY * sensitivity_;

    old_x_ = event.x();
    old_y_ = event.y();
    VectorsFromAngles();
}

void GLCamera::OnKeyboard(const QKeyEvent &event) {
    for (auto &_keystate : keystates_) {
        if (event.key() == _keystate.first) {
            _keystate.second = (event.type() == QEvent::KeyPress);
            break;
        }
    }
}

void GLCamera::animate(qint64 time_step) {
    double real_speed = (keystates_[keyconf_["boost"]]) ? 10 * speed_ : speed_;

    if (keystates_[keyconf_["left"]]) {
        theta_ += 10 * sensitivity_;
        VectorsFromAngles();
    }
    if (keystates_[keyconf_["right"]]) {
        theta_ -= 10 * sensitivity_;
        VectorsFromAngles();
    }
    if (keystates_[keyconf_["god"]]) {
        god_view_ = true;
    }
    if (keystates_[keyconf_["reset"]]) {
        phi_ = 0;
        theta_ = 0;
        position_ = QVector3D(1, 1, 1);
        phi_ = 0;
        theta_ = 0;
        god_view_ = false;
        VectorsFromAngles();
    }
    if (keystates_[keyconf_["forward"]]) {
        QVector3D old_position(position_);
        position_ += forward_ * (real_speed * time_step);
        position_ = verifyNewPosition(old_position, position_);
    }
    if (keystates_[keyconf_["backward"]]) {
        QVector3D old_position(position_);
        position_ -= forward_ * (real_speed * time_step);
        position_ = verifyNewPosition(old_position, position_);
    }
    if (keystates_[keyconf_["strafe_left"]]) {
        QVector3D old_position(position_);
        position_ += left_ * (real_speed * time_step);
        position_ = verifyNewPosition(old_position, position_);
    }
    if (keystates_[keyconf_["strafe_right"]]) {
        QVector3D old_position(position_);
        position_ -= left_ * (real_speed * time_step);
        position_ = verifyNewPosition(old_position, position_);

    }
    target_ = position_ + forward_;
}

void GLCamera::setPosition(const QVector3D &position) {
    position_ = position;
    target_ = position_ + forward_;
}

void GLCamera::VectorsFromAngles() {
    static const QVector3D up(0, 1, 0);
    if (phi_ > 89)
        phi_ = 89;
    else if (phi_ < -89)
        phi_ = -89;
    double r_temp = std::cos(phi_ * M_PI / 180);

    forward_.setY(sin(phi_ * M_PI / 180));
    forward_.setZ(r_temp * cos(theta_ * M_PI / 180));
    forward_.setX(r_temp * sin(theta_ * M_PI / 180));

    left_ = QVector3D::crossProduct(up, forward_); //TODO verify this
    left_.normalize();

    target_ = position_ + forward_;
}

void GLCamera::look() {
    if (god_view_) {
        gluLookAt(10, 15, 6,
                  10, 0, 6,
                  0, 0, 1);
    } else {
        gluLookAt(position_.x(), 1, position_.z(),
                  target_.x(), target_.y(), target_.z(),
                  0, 1, 0);
    }
}


/**
 * Check X and Z independently to see if they are valid.
 * @param old_position
 * @param calculated_new_position
 * @return QVector3D accepted_new_position
 */
QVector3D GLCamera::verifyNewPosition(const QVector3D &old_position, const QVector3D &calculated_new_position) {
    // init new position with old value before changing them
    QVector3D accepted_new_position = QVector3D(old_position.x(), old_position.y(), old_position.z());

    if (positionXisValid(calculated_new_position)) {
        accepted_new_position.setX(calculated_new_position.x());
    }
    if (positionZisValid(calculated_new_position)) {
        accepted_new_position.setZ(position_.z());
    }

    return accepted_new_position;
}

bool GLCamera::positionXisValid(const QVector3D &position) {
    float margin = 0.35;

    int rounded_x = static_cast<int>(std::round(position.x()));
    int rounded_z = static_cast<int>(std::round(position.z()));
    int floor_x = std::floor(position.x());
    int ceil_x = std::ceil(position.x());
    int floor_z = std::floor(position.z());
    int ceil_z = std::ceil(position.z());

    // ! FIX access out of range value if rounded, floor or ceil are bigger/lower thant the size of maze.
    if (rounded_x < 0 || rounded_x >= this->walls_[0].size() || floor_z < 0 || ceil_z >= this->walls_.size()
        || rounded_z < 0 || rounded_z >= this->walls_.size() || floor_x < 0 || ceil_x >= this->walls_[0].size()) {
        return false;
    }

    //Verify if in post
    if (std::abs(position.x() - rounded_x) < 0.35 && rounded_x % 2 == 0 && rounded_z % 2 == 0 &&
        std::abs(position.z() - rounded_z) < 0.35) {
        //std::cout << " X Futur Position X : " << position.X << " Y : " << position.Y << " Z : " << position.Z
        //          << std::endl;
        //std::cout << "Post at X  : " << rounded_x
        //          << "Post at Z : " << rounded_z << std::endl;

        //test if lest or right
        if (std::abs(position.z() - rounded_z) <= std::abs(position.x() - rounded_x)) {
            return false;

        }
    }

    //Verify future x position with walls
    if (std::abs(position.x() - rounded_x) < margin) { // Near a wall
        if ((this->walls_)[ceil_z][rounded_x] == true && (this->walls_)[floor_z][rounded_x] == true) {
            //std::cout << "Futur Position X : " << position.X << " Y : " << position.Y << " Z : " << position.Z
            //          << std::endl;
            //std::cout << "Wall at X  : " << rounded_x
            //          << " Ceil z : " << ceil_z
            //          << " Floor z : " << floor_z << std::endl;
            return false;
        }
    }
    return true;
}

bool GLCamera::positionZisValid(const QVector3D &position) {
    float margin = 0.35;

    int rounded_x = static_cast<int>(std::round(position.x()));
    int rounded_z = static_cast<int>(std::round(position.z()));
    int floor_x = std::floor(position.x());
    int ceil_x = std::ceil(position.x());
    int floor_z = std::floor(position.z());
    int ceil_z = std::ceil(position.z());

    // ! FIX access out of range value if rounded, floor or ceil are bigger/lower thant the size of maze.
    if (rounded_z < 0 || rounded_z >= this->walls_.size() || floor_x < 0 || ceil_x >= this->walls_[0].size()
        || rounded_x < 0 || rounded_x >= this->walls_[0].size() || floor_z < 0 || ceil_z >= this->walls_.size()) {
        return false;
    }

    //Verify if in post
    if (std::abs(position.z() - rounded_z) < 0.35 && rounded_x % 2 == 0 && rounded_z % 2 == 0 &&
        std::abs(position.x() - rounded_x) < 0.35) {
        //std::cout << "Z Futur Position X : " << position.X << " Y : " << position.Y << " Z : " << position.Z
        //          << std::endl;
        //std::cout << "Post at X  : " << rounded_x
        //          << "Post at Z : " << rounded_z << std::endl;

        //test if up or right
        if (std::abs(position.z() - rounded_z) >= std::abs(position.x() - rounded_x)) {
            return false;
        }
    }

    //Verify future Z position
    if (std::abs(position.z() - rounded_z) < margin) {
        if (((this->walls_)[rounded_z][ceil_x] == true && (this->walls_)[rounded_z][floor_x] == true)) {
            //std::cout << "Futur Position X : " << position.X << " Y : " << position.Y << " Z : " << position.Z
            //          << std::endl;
            //std::cout << "Wall at Z  : " << rounded_z
            //          << " Ceil x : " << ceil_x
            //          << " Floor x : " << floor_x << std::endl;
            return false;
        }
    }
    return true;
}

void GLCamera::set_walls(std::vector<std::vector<bool >> walls) {
    this->walls_ = std::move(walls);
}

const QVector3D &GLCamera::get_position() const {
    return position_;
}

const QVector3D &GLCamera::get_target() const {
    return target_;
}

void GLCamera::changeDirection(const QString &direction) {
    if (direction == "LEFT") {
        keystates_[keyconf_["left"]] = true;
    } else if (direction == "RIGHT") {
        keystates_[keyconf_["right"]] = true;
    } else if (direction == "UP") {
        keystates_[keyconf_["forward"]] = true;
    } else if (direction == "DOWN") {
        keystates_[keyconf_["backward"]] = true;
    } else {
        for (auto &_keystate : keystates_) {
            _keystate.second = false;
        }
    }
}


GLCamera::~GLCamera() = default;