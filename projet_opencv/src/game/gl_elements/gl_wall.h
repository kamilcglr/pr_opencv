/**
 * @file gl_wall.h
 * @brief This class represents an openGL wall.
 * @note For better performance, do not user double but float.
 */
#ifndef PR_OPENCV_GL_WALL_H
#define PR_OPENCV_GL_WALL_H

#include <utility>
#include <QtGui/QVector3D>

/**
 * @class Represents a 3d rectangle. The walls can be normal walls or posts. When it is a post, the length is the same
 * on each face.
 * @note When the wall is at x = 0, z = 0 :
 * Front is the original thin face.
 * Behind is the thin face in x = length.
 * Left is the left side of the wall when viewed from Front.
 * Right is the right side of the wall when viewed from the front.
 */
class GLWall {
public:
    /**
     * Constructor
     * @param position_x1
     * @param position_z1
     * @param vertical if true, the wall is perpendicular to x axis, else it is parallel to z axis in openGL
     * @param thickness
     * @param is_post if true, the length of each faces is the same. (Cubic wall)
     * @param texture pointer to the texture applied to faces
     */
    GLWall(float position_x1, float position_z1, bool vertical,
           float thickness, bool is_post, GLuint *texture);

    /**
     * Delete quadric and light arrays.
     */
    ~GLWall();

    /**
     * Display the wall on its correct position. If the wall is vertical, we make a 90° rotation.
     */
    virtual void display() const;

private:
    GLuint *texture; /**< Pointer to the texture applied to faces */
    bool vertical_; /**< if true : perpendicular to x, if false parallel to x */
    float position_x1_;
    float position_z1_;
    float elevation_;  /**< Elevation of the wall */
    float length_;
    float thickness_;

    std::pair<float, float> texture_bl_; /**< Coordinate of the texture to bottom left */
    std::pair<float, float> texture_br_; /**< Coordinate of the texture to bottom right */
    std::pair<float, float> texture_tl_; /**< Coordinate of the texture to top left */
    std::pair<float, float> texture_tr_; /**< Coordinate of the texture to top right */

    GLfloat *ambient_diffuse_color_;
    GLfloat *specular_color_;
    GLfloat *emitting_color_;

    QVector3D bottom_1_;
    QVector3D bottom_2_;
    QVector3D bottom_3_;
    QVector3D bottom_4_;

    QVector3D top_1_;
    QVector3D top_2_;
    QVector3D top_3_;
    QVector3D top_4_;

    QVector3D left_1_;
    QVector3D left_2_;
    QVector3D left_3_;
    QVector3D left_4_;

    QVector3D right_1_;
    QVector3D right_2_;
    QVector3D right_3_;
    QVector3D right_4_;

    QVector3D front_1_;
    QVector3D front_2_;
    QVector3D front_3_;
    QVector3D front_4_;

    QVector3D back_1_;
    QVector3D back_2_;
    QVector3D back_3_;
    QVector3D back_4_;
};


#endif //PR_OPENCV_GL_WALL_H
