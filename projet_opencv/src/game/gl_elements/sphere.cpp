#include <QtGui/QImage>
#include <QtOpenGL/QGLWidget>
#include <iostream>
#include "sphere.h"
#include <utility>

Sphere::Sphere(QVector3D position, float radius, bool rotate, GLuint *texture,
               std::map<std::string, float> light_attenuation)
        : position_(position), radius_(radius), rotate_(rotate), light_attenuation_(std::move(light_attenuation)) {

    this->texture_ = texture;
    this->glu_quadric_ = gluNewQuadric();
    this->is_visible_ = true;

    this->ambient_color_ = new GLfloat[4]{1.0, 1.0, 1.0, 1.0};
    this->diffuse_color_ = new GLfloat[4]{1.0, 1.0, 1.0, 1.0};
    this->specular_color_ = new GLfloat[4]{1.0, 1.0, 1.0, 1.0};
    this->emitting_color_ = new GLfloat[4]{1.0, 1.0, 1.0, 1.0};

    glBindTexture(GL_TEXTURE_2D, this->texture_[0]);
}

void Sphere::display(const qint64 time) {
    GLfloat light_position[] = {this->position_.x(), this->position_.y(), this->position_.z(), 1};

    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_color_);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_color_);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular_color_);

    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, light_attenuation_["GL_CONSTANT_ATTENUATION"]);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, light_attenuation_["GL_LINEAR_ATTENUATION"]);
    glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, light_attenuation_["GL_QUADRATIC_ATTENUATION"]);

    if (is_visible_) {
        gluQuadricTexture(this->glu_quadric_, GL_TRUE);
        glBindTexture(GL_TEXTURE_2D, this->texture_[0]);

        glPushMatrix();
        glTranslatef(this->position_.x(), this->position_.y(), this->position_.z());
        gluQuadricDrawStyle(this->glu_quadric_, GLU_FILL);

        if (rotate_) {
            glRotatef(time / 50.0, 0, 1, 0); //Keep this rotation first
            glRotatef(270, 1, 0, 0);
        }
        glMaterialfv(GL_FRONT, GL_EMISSION, emitting_color_);

        gluSphere(this->glu_quadric_, this->radius_, 20, 20);
        glPopMatrix();
    }
}

Sphere::~Sphere() {
    gluDeleteQuadric(this->glu_quadric_);
    delete[]ambient_color_;
    delete[]diffuse_color_;
    delete[]specular_color_;
    delete[]emitting_color_;
}

QVector3D Sphere::getPosition() {
    return position_;
}

void Sphere::setIsVisible(bool isVisible) {
    is_visible_ = isVisible;
}

void Sphere::setPosition(QVector3D new_position) {
    this->position_ = new_position;
}
