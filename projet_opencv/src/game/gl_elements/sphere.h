/**
 * @file sphere.h
 * @brief Sphere object emitting light.
 * @note For better performance, do not user double but float.
 */
#ifndef PR_OPENCV_SPHERE_H
#define PR_OPENCV_SPHERE_H

#include <GL/glu.h>
#include <QtCore/QTimer>
#include <QtGui/QVector3D>

/**
 * @class Object to display a textured gluQuadric in openGL.
 */
class Sphere {

public:
    /**
     * Create a gluQuadric sphere and initialize its fields.
     * @param position position in openGL references
     * @param radius
     * @param rotate if true, the sphere will rotate on each drawing
     * @param light_attenuation map with the parameters of light attenuation
     * @param texture applied to the quadric
     */
    Sphere(QVector3D position, float radius, bool rotate, GLuint *texture,
           std::map<std::string, float> light_attenuation);

    /**
     * Delete quadric and arrays.
     */
    ~Sphere();

    /**
     * Set the position of the sphere in openGL references.
     * @param new_position
     */
    void setPosition(QVector3D new_position);

    /**
     * Display the sphere in its position, make a rotation if necessary.
     * @param time_elapsed
     */
    virtual void display(qint64 time_elapsed);

    /**
     * Get the position of the sphere in openGL references.
     * @return QVector3D position of the sphere
     * */
    QVector3D getPosition();

    /**
     * If true, the sphere will be drawn. Else, only the light will by present.
     * When the sphere is found, only the light is displayed.
     * @param isVisible
     */
    void setIsVisible(bool isVisible);

private:
    QVector3D position_; /**< Position x,y,z. @warning x is length and z is height, y is elevation */
    bool rotate_; /**< If true, the sphere will rotate on each update */
    float radius_;
    bool is_visible_;

    GLUquadric *glu_quadric_; /**< Will represent the visual element in scene */

    GLfloat *ambient_color_;
    GLfloat *diffuse_color_;
    GLfloat *specular_color_;
    GLfloat *emitting_color_;

    std::map<std::string, float> light_attenuation_; /**< Attenuation of light. Get a look to
 * http://learnwebgl.brown37.net/09_lights/lights_attenuation.html for better understanding of values*/

    GLuint *texture_; /**< Pointer to the texture applied to this glu_quadric_ */
};


#endif //PR_OPENCV_SPHERE_H
