#define _USE_MATH_DEFINES

#include "gl_maze.h"
#include <QThread>
#include <QDesktopWidget>
#include <QApplication>
#include <QScreen>
#include <iostream>
#include <QtWidgets/QWidget>

GLMaze::GLMaze(QWidget *parent) : QGLWidget(parent) {
}

void GLMaze::initializeMaze(int width, int height) {
    // Generation of the maze_, display it in console
    this->maze_ = new Maze(width, height);
    this->maze_->generate();
    this->maze_->display();

    //Create wall database
    createWallsVectors();

    gl_camera_ = new GLCamera(QVector3D(0.5, 1, 0.5));
    gl_camera_->set_walls(walls_);
    gl_camera_->setPosition(QVector3D(this->maze_->getUserPosition().first * factor_scale_ - 0.5, 1,
                                      this->maze_->getUserPosition().second * factor_scale_ - 0.5));

    timer_.start(interval_);
    elapsed_timer_since_launch.start();

    connect(&timer_, &QTimer::timeout, [&] {
        time_elapsed_ = elapsed_timer_.elapsed();
        elapsed_timer_.restart();
        gl_camera_->animate(time_elapsed_);
        this->updateGL();
        gameStatus();
        emit positionChange(
            QPointF(gl_camera_->get_position().x() / static_cast<float>(factor_scale_),
                    gl_camera_->get_position().z() / static_cast<float>(factor_scale_)),
            QPointF(gl_camera_->get_target().x() / static_cast<float>(factor_scale_),
                    gl_camera_->get_target().z() / static_cast<float>(factor_scale_)));
    });
}

void GLMaze::initializeGL() {
    // Background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    // Activate lightning
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);

    // Activate z-Buffer
    glEnable(GL_DEPTH_TEST);

    // Activate Texture
    glEnable(GL_TEXTURE_2D);

    // ! Create Elements after !
    createGLElement();
}

void GLMaze::resizeGL(int width, int height) {
    // viewport definition
    glViewport(0, 0, width, height);

    // projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(70, (double) width / height, 0.001, 1000);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void GLMaze::createGLElement() {
    loadTextures();
    createGLWalls();

    std::map<std::string, float> sphere_attenuation = {
            {"GL_CONSTANT_ATTENUATION",  1},
            {"GL_LINEAR_ATTENUATION",    0.1},
            {"GL_QUADRATIC_ATTENUATION", 0.1}
    };

    std::map<std::string, float> moon_attenuation = {
            {"GL_CONSTANT_ATTENUATION",  1},
            {"GL_LINEAR_ATTENUATION",    1},
            {"GL_QUADRATIC_ATTENUATION", 1}
    };

    sphere_ = new Sphere(QVector3D(float(this->maze_->getSpherePosition().first * factor_scale_ - 1), 1,
                                   float(this->maze_->getSpherePosition().second * factor_scale_ - 1)), 0.45, true,
                         &this->textures_[2], sphere_attenuation);

    moon_ = new Sphere(QVector3D(0, 10, 0), 0.5, false, &this->textures_[3], moon_attenuation);

    light1_ = new GLfloat[4]{static_cast<GLfloat>(this->maze_->getWidth() * 4), 13,
                             static_cast<GLfloat>(this->maze_->getHeight() * 4), 1};
}

void GLMaze::createWallsVectors() {
    walls_ = std::vector<std::vector<bool>>(this->maze_->getHeight() * factor_scale_ + 1,
                                            std::vector<bool>(this->maze_->getWidth() * factor_scale_ + 1, false));

    int length = 2;
    int temp_z = 0;
    int temp_x = 0;

    temp_x = 0;
    // first walls of top
    for (int x = 0; x < this->maze_->getWidth() * factor_scale_; x++) {
        if (this->maze_->get_grid()[0][x].isFrontier(Cell::N)) {
            walls_[0][temp_x] = true;
            walls_[0][temp_x + 1] = true;
            walls_[0][temp_x + 2] = true;
        }
        temp_x = temp_x + length;
    }

    // other lines
    for (int z = 0; z < this->maze_->getHeight(); z++) {
        if (this->maze_->get_grid()[z][0].isFrontier(Cell::W)) {
            // Beginning of line
            walls_[temp_z][0] = true;
            walls_[temp_z + 1][0] = true;
            walls_[temp_z + 2][0] = true;
        }

        temp_x = 0;
        for (int x = 0; x < this->maze_->getWidth(); x++) {
            if (this->maze_->get_grid()[z][x].isFrontier(Cell::E)) {
                walls_[temp_z][temp_x + length] = true;
                walls_[temp_z + 1][temp_x + length] = true;
                walls_[temp_z + 2][temp_x + length] = true;
            }
            temp_x = temp_x + length;
        }

        temp_x = 0;
        for (int x = 0; x < this->maze_->getWidth(); x++) {
            if (this->maze_->get_grid()[z][x].isFrontier(Cell::S)) {
                walls_[temp_z + length][temp_x] = true;
                walls_[temp_z + length][temp_x + 1] = true;
                walls_[temp_z + length][temp_x + 2] = true;
            }
            temp_x = temp_x + length;
        }
        temp_z = temp_z + length;
    }
}

void GLMaze::paintGL() {
    // Reinitialisation des tampons
    glClear(GL_DEPTH_BUFFER_BIT);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    // use this function for opengl target camera
    gl_camera_->look();

    if (sphere_found_) {
        sphere_->setPosition(this->getGlCamera()->get_position());
    }
    sphere_->display(elapsed_timer_since_launch.elapsed());

    displayWalls();
    drawFloor();
    drawSky();
    moon_->display(elapsed_timer_since_launch.elapsed());
}

void GLMaze::displayWalls() {
    std::list<GLWall>::const_iterator iterator;
    for (iterator = gl_walls_.begin(); iterator != gl_walls_.end(); ++iterator) {
        iterator->display();
    }
}

void GLMaze::drawGrid() {
    for (int i = 0; i <= 20; i += 1) {
        glBegin(GL_LINES);
        glColor3ub(150, 190, 150);
        glVertex3f(0, 0, i);
        glVertex3f(20, 0, i);
        glVertex3f(i, 0, 0);
        glVertex3f(i, 0, 20);
        glEnd();
    }
}

/**
 * Display the walls_ vector to check if it the same as this->maze_->grid_.
 */
void GLMaze::displayWallsVectors() {
    for (int z = 0; z < this->walls_.size(); z++) {
        for (int x = 0; x < this->walls_[z].size(); x++) {
            if (this->walls_[z][x] == true) {
                std::cout << "#";
            } else {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }
}

/**
 *
 * @return maze_z
 */
Maze *GLMaze::getMaze() const {
    return maze_;
}

void GLMaze::changeMouseTacking(bool state) {
/*
     * This property holds whether mouse tracking is enabled for the widget.
     * If mouse tracking is disabled (the default), the widget only receives mouse move events when at least one mouse button is pressed while the mouse is being moved.
     * If mouse tracking is enabled, the widget receives mouse move events even if no buttons are pressed.
     */
    mouse_events_ = state;
    setMouseTracking(state);
    QPoint glob = mapToGlobal(QPoint(this->width() / 2, this->height() / 2));
    QCursor::setPos(glob);
}

GLCamera *GLMaze::getGlCamera() const {
    return gl_camera_;
}

void GLMaze::keyPressEvent(QKeyEvent *event) {
    gl_camera_->OnKeyboard(*event);
    event->accept();
}

void GLMaze::keyReleaseEvent(QKeyEvent *event) {
    gl_camera_->OnKeyboard(*event);
    event->accept();
}

void GLMaze::mouseMoveEvent(QMouseEvent *event) {
    if (mouse_events_) {
        gl_camera_->OnMouseMotion(*event);
        event->accept();
    }
}

void GLMaze::loadTextures() {
    std::vector<QImage> images;

    images.push_back(QGLWidget::convertToGLFormat(QImage("res/textures/wall_brick_gray.jpg")));
    images.push_back(QGLWidget::convertToGLFormat(QImage("res/textures/floor_paved.jpg")));
    images.push_back(QGLWidget::convertToGLFormat(QImage("res/textures/telecom_logo.png")));
    images.push_back(QGLWidget::convertToGLFormat(QImage("res/textures/moon_1k.jpg")));
    glGenTextures(4, this->textures_);

    for (int i = 0; i < images.size(); ++i) {
        glBindTexture(GL_TEXTURE_2D, this->textures_[i]);

        glTexImage2D(GL_TEXTURE_2D, 0, 4, images[i].width(), images[i].height(), 0, GL_RGBA, GL_UNSIGNED_BYTE,
                     images[i].bits());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
}

void GLMaze::showExit() {
    this->maze_->generateExit();

    this->exit_position_.first = this->maze_->getExitPosition().first * factor_scale_;
    this->exit_position_.second = this->maze_->getExitPosition().second * factor_scale_;
    exit_position_.first += 1;
    exit_position_.second += 1;

    this->walls_.clear();
    this->gl_walls_.clear();

    createWallsVectors();
    createGLWalls();
}

void GLMaze::gameStatus() {
    if (sphere_found_) { // Sphere is found, we test if we are near exit
        if (gl_camera_->get_position().x() > exit_position_.first - 0.5
            && gl_camera_->get_position().x() < exit_position_.first + 0.5
            && gl_camera_->get_position().z() > exit_position_.second - 0.5
            && gl_camera_->get_position().z() < exit_position_.second + 0.5) {
            emit endOfGame(true);
        }
    } else { // Sphere is not found, we test if current position is on it
        if (gl_camera_->get_position().x() > sphere_->getPosition().x() - 1
            && gl_camera_->get_position().x() < sphere_->getPosition().x() + 1
            && gl_camera_->get_position().z() > sphere_->getPosition().z() - 1
            && gl_camera_->get_position().z() < sphere_->getPosition().z() + 1) {
            sphere_found_ = true;
            this->sphere_->setIsVisible(false);
            showExit();
            emit sendMessage(QString("Vous avez trouvé la sphère, cherchez la sortie !"));
            emit nextMusic();
        }
    }

}

void GLMaze::createGLWalls() {
    int length = factor_scale_;
    int temp_z = 0;
    int temp_x = 0;

    // first wall of top
    for (int x = 0; x < this->maze_->getWidth(); x++) {
        if (this->maze_->get_grid()[0][x].isFrontier(Cell::N)) {
            gl_walls_.emplace_back(temp_x, 0, false, 0.25, false, textures_);
        }
        temp_x = temp_x + length;
    }

    // other lines
    for (int z = 0; z < this->maze_->getHeight(); z++) {
        // Beginning of line
        if (this->maze_->get_grid()[z][0].isFrontier(Cell::W)) {
            gl_walls_.emplace_back(0, temp_z, true, 0.25, false, textures_);
        }

        temp_x = 0;
        for (int x = 0; x < this->maze_->getWidth(); x++) {
            if (this->maze_->get_grid()[z][x].isFrontier(Cell::E)) {
                gl_walls_.emplace_back(temp_x + length, temp_z, true, 0.25, false, textures_);
            }
            temp_x = temp_x + length;
        }

        temp_x = 0;
        for (int x = 0; x < this->maze_->getWidth(); x++) {
            if (this->maze_->get_grid()[z][x].isFrontier(Cell::S)) {
                gl_walls_.emplace_back(temp_x, temp_z + length, false, 0.25, false, textures_);
            }
            temp_x = temp_x + length;
        }
        temp_z = temp_z + length;
    }

    // Create all posts
    for (int z = 0; z <= this->maze_->getHeight() * factor_scale_; z = z + length) {
        for (int x = 0; x <= this->maze_->getWidth() * factor_scale_; x = x + length) {
            gl_walls_.emplace_back(x, z, false, 0.25, true, textures_);
        }
    }
}

void GLMaze::drawFloor() {
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, floor_ambient_diffuse_);
    glMaterialfv(GL_FRONT, GL_SPECULAR, floor_specular_);

    glBindTexture(GL_TEXTURE_2D, this->textures_[1]);
    for (int i = -100; i < 100; i++) {
        for (int j = -100; j < 100; j++) {
            glBegin(GL_QUADS);
            glNormal3f(0, 1, 0);
            glTexCoord2f(0, 0);
            glVertex3f(i, 0, j);
            glTexCoord2f(0, 1);
            glVertex3f(i + 1, 0, j);
            glTexCoord2f(1, 1);
            glVertex3f(i + 1, 0, j + 1);
            glTexCoord2f(1, 0);
            glVertex3f(i, 0, j + 1);
            glEnd();
        }
    }
}

void GLMaze::drawSky() {
    glLightfv(GL_LIGHT1, GL_POSITION, light1_);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient_);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse_);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular_);

    glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1);
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.0);
    glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.05);

    glMaterialfv(GL_FRONT, GL_AMBIENT, sky_ambient_);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, sky_diffuse_);
    glMaterialfv(GL_FRONT, GL_SPECULAR, sky_specular_);


    for (int i = -200; i < 200; i += 5) {
        for (int j = -200; j < 200; j += 5) {
            glBegin(GL_QUADS);
            glNormal3f(0.0, -1.0, 0.0);
            glVertex3f(i, 15, j);
            glVertex3f(i + 5, 15, j);
            glVertex3f(i + 5, 15, j + 5);
            glVertex3f(i, 15, j + 5);
            glEnd();
        }
    }
}

QTimer &GLMaze::getTimer() {
    return timer_;
}
