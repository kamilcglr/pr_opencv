/**
 * @file game_window.h
 * @brief This class is associated with a game_window.ui to display the game, the top view and the webcam output.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "camera/camera_manager.h"
#include <QMainWindow>
#include <QtCore/QElapsedTimer>
#include <QtCore/QTimer>
#include <QtWidgets/QLabel>
#include <QtMultimedia/QMediaPlaylist>
#include <QtMultimedia/QMediaPlayer>

QT_BEGIN_NAMESPACE
namespace Ui { class GameWindow; }
QT_END_NAMESPACE

class GameWindow : public QMainWindow {
Q_OBJECT

public:
    /**
     * Entry point of game, create the glMaze with its size in parameters.
     * Start all timers and connect slots.
     * @param maze_width
     * @param maze_height
     * @param parent
     */
    explicit GameWindow(int maze_width = 10, int maze_height = 6, QWidget *parent = nullptr);

    ~GameWindow() override;

    void resizeEvent(QResizeEvent *) override;

signals:

    /**
     * Informs menu_window to enable play Button because game has terminated or user exited.
     */
    void enablePlayButton();

private:
    /**
     * Return a QString with a human readable elapsed time.
     * @return
     */
    QString elapsedTimeSinceLaunchFormatted();

    /**
     * Initialize the 3 labels in status bar.
     * The first is for camera information
     * The second is for game information
     * The third displays the elapsed time
     */
    void initStatusBar();

    /**
     * Load songs from resources and launch the playlist;
     */
    void initMusic();

private slots:

    /**
     * Handle keyPressEvent. Propagate them to gl_maze.
     * If Escape is pressed, exit the game and go to main menu.
     */
    void keyPressEvent(QKeyEvent *event) override;

    /**
     * Handle keyReleaseEvent. Propagate them to gl_maze.
     */
    void keyReleaseEvent(QKeyEvent *event) override;

    /**
     * When the user exit the game, either the exit is found or it has pressed Escape.
     * Stop the music, the camera and game refresh. Because of camera_manager being in another thread, wait 5 sec to
     * terminate it if it is not immediate.
     * @param exit_found
     */
    void onEndOfGame(bool exit_found);

    /**
     * Print the message to status bar.
     * @param message
     */
    void onGameMessage(const QString &message);

    /**
     * Print the direction from camera to status bar.
     * Launch the top_view timer when the user do not move.
     * @param dir
     */
    void directionMove(const QString &dir);

private:
    Ui::GameWindow *ui;
    CameraManager camera_manager_; /**< Thread to detect user face movements */
    QTimer top_view_timer_; /**< Permit to display the top_view only if user is in idle position ofr a certain amount of time */

    bool mouse_movements_ = false; /**< If true, the mouse_movements are enabled */

    QLabel *direction_label_;
    QLabel *time_label_;
    QLabel *game_label_;

    QTimer update_chrono_; /**< Permit to update time on status bar */
    QElapsedTimer elapsed_timer_from_launch_; /**< Permit to measure time passed in game */

    // Music
    QMediaPlaylist *playlist_; /**< Contains all songs */
    QMediaPlayer *music_;  /**< Play the playlist songs */
};

#endif // MAINWINDOW_H
