/**
 * @file top_view.h
 * @brief QWidget to display the position of the user in the game.
 */
#ifndef PR_OPENCV_TOP_VIEW_H
#define PR_OPENCV_TOP_VIEW_H

#include <QtWidgets/QWidget>
#include <game/maze_generation/maze.h>
#include <QtGui/QVector3D>
#include <QtCore/QTimer>

/**
 * @Class Represent the maze in 2D and the user position. Connected with GLCamera.
 */
class TopView : public QWidget {
Q_OBJECT
public :
    /**
     * @Constructor
     * @param parent
     */
    explicit TopView(QWidget *parent = nullptr);

    /**
     * Set this->maze_ to game maze pointer.
     * @param maze pointer to the original maze
     */
    void setMaze(Maze *maze);

public slots:

    /**
     * Sets the new position of camera and repaint the maze.
     * @param position_camera
     * @param target_camera
     */
    void setNewPositionAndTarget(QPointF position_camera, QPointF target_camera);

private:

    /**
     * Draw the maze and the user position.
     */
    void paintEvent(QPaintEvent *) override;

private:
    Maze *maze_; /**< Pointer to the generated maze */
    QPointF position_camera_ = QPointF(0, 0); /**< x : position x in GLMaze, y : position z in GLMaze */
    QPointF target_camera_ = QPointF(0, 0); /**< x : position x in GLMaze, y : position z in GLMaze */
};

#endif //PR_OPENCV_TOP_VIEW_H