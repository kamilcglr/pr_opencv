/**
 * @file maze.h
 * @brief An implementation of Prim's algorithm for generating mazes, from <http://weblog.jamisbuck.org/2011/1/10/maze-generation-prim-s-algorithm>
 *
 * C++ implementation by C. Ducottet
 * Modifications and Mersenne random added by K. CAGLAR
**/
#ifndef MAZE_H
#define MAZE_H

#include "cell.h"
#include <vector>
#include <list>
#include <utility>
#include <random>

/**
 * @class Generate the maze and provides methods to generate positions of sphere, exit and user.
 * Use of Mersenne Twister algorithm instead of rand().
 * Position are translated to x and z in openGL.
 */
class Maze {
public:

    /**
     * Initialise attributes of maze and random devices.
     * For windows, we use the wincrypt API.
     * For linux, we use std::random_device that get random seed from /dev/random
     * @param width
     * @param height
     */
    Maze(int width, int height);

    /**
     * Initialize the grid object with size attributes (height_ and width_).
     * A grid is composed bu Cells.
     */
    void reInit();

    /**
     * Display the grid in console.
     * @param pause if true, clear the console on windows
     */
    void display(bool pause = false);

    /**
     * Generate the maze with Prim's algorithm. Find the walls of each cell. Massive use of uniform distribution with mersenne twister for random.
     * @param show if true, display the maze before generation.
     */
    void generate(bool show = false);

    /**
     * Add the position of a cell to the frontier list
     * @param p position of the cell
     * @param frontier list of frontiers
     */
    void addFrontier(std::pair<int, int> p, std::list<std::pair<int, int>> &frontier);

    /**
     * Mark a cell and add it to the frontier list.
     * @param p position of the cell
     * @param frontier list of frontiers
     */
    void mark(std::pair<int, int> p, std::list<std::pair<int, int>> &frontier);

    /**
     * Get the neighbours cells of the cell passed as parameter.
     * @param p position of the cell
     * @return a list of position of the neighbours cells.
     */
    std::list<std::pair<int, int>> neighbors(std::pair<int, int> p);

    /**
     * Get the Direction in a cell between two cells.
     * @param f the cell we consider
     * @param t the other cell
     * @return Cell::Direction enum type in cell.h representing the direction
     */
    static Cell::Direction direction(std::pair<int, int> f, std::pair<int, int> t);

    /**
     * Generate random exit position b creating an empty wall on the borders. The random part uses Mersenne twister.
     * @warning DO NOT USE before wall initialization.
     */
    void generateExit();

    /**
     * Generate user and sphere position. The random part uses Mersenne twister.
     * @warning DO NOT USE before wall initialization.
     */
    void generateUserAndSpherePosition();

    /**
     * Get the reference to the grid of cells.
     * @return 2D vector of cell
     */
    [[nodiscard]] const std::vector<std::vector<Cell>> &get_grid() const;

    /**
     * Get the sphere position
     * @return pair of int. First : x on length. Second : z on height.
     */
    [[nodiscard]] const std::pair<int, int> &getSpherePosition() const;

    /**
    * Get the user position
    * @return pair of int. First : x on length. Second : z on height.
    */
    [[nodiscard]] const std::pair<int, int> &getUserPosition() const;

    /**
    * Get the exit position
    * @return pair of int. First : x on length. Second : z on height.
    */
    [[nodiscard]] const std::pair<int, int> &getExitPosition() const;

    /**
     *
     * @return int
     */
    int getWidth();

    /**
     *
     * @return int
     */
    int getHeight();

private:

    std::mt19937 mersenne_twister_; /**< Random number generator
 * https://stackoverflow.com/questions/7114043/random-number-generation-in-c11-how-to-generate-how-does-it-work*/

    std::vector<std::vector<Cell>> grid_;  /**< 2D vector of Cell */

    int height_; /**< Height of the grid, z in openGL */
    int width_; /**< Length of the grid, x in openGL */

    std::pair<int, int> sphere_position_; /**< Generated sphere position, first : x, second : z */
    std::pair<int, int> user_position_; /**< Generated user position, first : x, second : z */
    std::pair<int, int> exit_position_; /**< Generated exit position, first : x, second : z */
};

#endif // MAZE_H

