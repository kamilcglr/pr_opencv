/**
 * @file cell.h
 * @brief An implementation of Prim's algorithm for generating mazes from <http://weblog.jamisbuck.org/2011/1/10/maze_-generation-prim-s-algorithm>
 *
 * C++ implementation by C. Ducottet
**/

#ifndef CELL_H
#define CELL_H

#include <string>

/**
 * @class The maze is composed by a grid of Cells.
 */
class Cell {
public:
    /**
     * @enum Each cell has a special type for Prim's algorithm.
     */
    enum Type {
        EMPTY, /**< Cell is empty */
        MARKED, /**< Cell is checked in Prim's algorithm */
        FRONTIER, /**< Cell is a frontier */
    };

    /**
     * @enum Directions are translated in N, E, S, W for more readability.
     */
    enum Direction {
        N = 1,
        S = 3,
        E = 0,
        W = 2
    };

    /**
     *
     * @return int the value of the cell
     */
    [[nodiscard]] int getValue() const { return value_; }

    /**
    * Change the value of a cell
    * @param type int
    */
    void setValue(int type) { value_ = type; }

    /**
     * Check if there is a wall on the requested side
     * @param d Direction to check
     * @return bool true when there is a frontier, false otherwise
     */
    [[nodiscard]] bool isFrontier(Direction d) const { return frontier_[d]; }

    /**
     * Set the status of a wall
     * @param d the direction
     * @param state boolean true if wall, false otherwise
     */
    void setFrontier(Direction d, bool state) { frontier_[d] = state; }

private:
    int value_ = EMPTY;  /**< Value of the cell */

    bool frontier_[4] = {true, true, true, true};  /**< State of the 4 walls, true by default */
};

#endif // CELL_H
