#include<iostream>
#include "cell.h"
#include "maze.h"

#ifdef _linux_
#elif _WIN32
#include <windows.h>
#include <wincrypt.h>
#include <cstdint>
#include <algorithm> //keep this for windows
#endif


using namespace std;

using Point = pair<int, int>;

Maze::Maze(int width, int height)
        : height_(height), width_(width) {
#if ((defined(_WIN32) || defined(_WIN64)) && !(defined(_MSCVER))) // Get seed from Windows API
    uint64_t ret;
    HCRYPTPROV hp = 0;
    CryptAcquireContext(&hp, 0, 0, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);
    CryptGenRandom(hp, sizeof(ret), reinterpret_cast<uint8_t*>(&ret));
    CryptReleaseContext(hp, 0);
    mersenne_twister_ = std::mt19937(ret);

#else // Get seed from /dev/random
    // Initialize random seed
    std::random_device dev;

    // Random generator - Mersenne Twister algorithm
    mersenne_twister_ = std::mt19937(dev());

#endif //(defined(_WIN32) || defined(_WIN64)) && !(defined(_MSCVER))
}

void Maze::reInit() {
    grid_ = vector<vector<Cell>>(height_, vector<Cell>(width_));
}

void Maze::addFrontier(pair<int, int> p, list<pair<int, int>> &frontier) {
    if (p.first >= 0 && p.second >= 0 && p.second < height_ && p.first < width_
        && grid_[p.second][p.first].getValue() == 0) {
        grid_[p.second][p.first].setValue(Cell::FRONTIER);
        frontier.push_back(p);
    }
}

void Maze::mark(pair<int, int> p, list<pair<int, int> > &frontier) {
    grid_[p.second][p.first].setValue(Cell::MARKED);
    addFrontier(Point(p.first - 1, p.second), frontier);
    addFrontier(Point(p.first + 1, p.second), frontier);
    addFrontier(Point(p.first, p.second - 1), frontier);
    addFrontier(Point(p.first, p.second + 1), frontier);
}

list<Point> Maze::neighbors(Point p) {
    list<Point> n;
    if (p.first > 0 && grid_[p.second][p.first - 1].getValue() == Cell::MARKED)
        n.emplace_back(p.first - 1, p.second);
    if (p.first + 1 < width_ && grid_[p.second][p.first + 1].getValue() == Cell::MARKED)
        n.emplace_back(p.first + 1, p.second);
    if (p.second > 0 && grid_[p.second - 1][p.first].getValue() == Cell::MARKED)
        n.emplace_back(p.first, p.second - 1);
    if (p.second + 1 < height_ && grid_[p.second + 1][p.first].getValue() == Cell::MARKED)
        n.emplace_back(p.first, p.second + 1);
    return n;
}

Cell::Direction Maze::direction(Point f, Point t) {
    if (f.first < t.first) return Cell::E;
    else if (f.first > t.first) return Cell::W;
    else if (f.second < t.second) return Cell::S;
    else return Cell::N;
}

void Maze::display(bool pause) {
    int i, j;
    string cell[3] = {"..", "  ", "()"};

    if (pause) system("cls"); // use "clear" under linux

    // Print the first line
    for (j = 0; j < width_; j++) {
        if (grid_[0][j].isFrontier(Cell::N)) {
            cout << "+--";
        } else {
            cout << "   ";
        }
    }
    cout << '+' << endl;

    // Print other lines
    for (i = 0; i < height_; i++) {
        // Beginning of line
        if (grid_[i][0].isFrontier(Cell::W)) {
            cout << '|';
        } else {
            cout << ' ';
        }
        // Print cells
        for (j = 0; j < width_; j++) {
            cout << cell[grid_[i][j].getValue()];
            if (grid_[i][j].isFrontier(Cell::E)) cout << '|';
            else cout << ' ';
        }
        cout << endl;
        // Beginning of line
        cout << '+';
        // Print horizontal frontier_
        for (j = 0; j < width_; j++) {
            if (grid_[i][j].isFrontier(Cell::S)) cout << "--";
            else cout << "  ";
            cout << '+';
        }
        cout << endl;
    }
}

void Maze::generate(bool show) {
    std::uniform_int_distribution<int> uniform_width_int_distribution(0, width_ - 1);
    std::uniform_int_distribution<int> uniform_height_int_distribution(0, height_ - 1);

    list<Point> frontier;

    // Initialize cells if the maze_ was already generated
    reInit();

    // Mark a random cell and add the frontier_ cells to the list
    mark(Point(uniform_width_int_distribution(mersenne_twister_), uniform_height_int_distribution(mersenne_twister_)),
         frontier);

    // display
    if (show) display(true);

    while (!frontier.empty()) {
        // Take a random frontier_ cell f (from)
        auto randPos = frontier.begin();
        std::uniform_int_distribution<int> uniform_frontier_int_distribution(0, frontier.size() - 1);
        advance(randPos, uniform_frontier_int_distribution(mersenne_twister_));
        Point f = *randPos;
        frontier.erase(randPos);

        // Take a random neighbor t (to) of that cell
        list<Point> n = neighbors(f);
        randPos = n.begin();
        std::uniform_int_distribution<int> uniform_neighbors_int_distribution(0, n.size() - 1);
        advance(randPos, uniform_neighbors_int_distribution(mersenne_twister_));
        Point t = *randPos;

        // Carve a passage from f to t
        Cell::Direction d = direction(f, t);
        grid_[f.second][f.first].setFrontier(d, false);
        grid_[t.second][t.first].setFrontier(Cell::Direction((d + 2) % 4), false);

        // Mark the cell and add the frontier_ cells to the list
        mark(f, frontier);

        // display
        if (show) display(true);
    }
    generateUserAndSpherePosition();
}

int Maze::getWidth() {
    return width_;
}

int Maze::getHeight() {
    return height_;
}

const vector<vector<Cell>> &Maze::get_grid() const {
    return grid_;
}

void Maze::generateExit() {
    // Random exit position :
    // 0 -> TOP
    // 1 -> RIGHT
    // 2 -> BOTTOM
    // 3 -> LEFT
    std::uniform_int_distribution<int> uniform_int_wall_distribution(0, 3);
    int wall_position = uniform_int_wall_distribution(mersenne_twister_);

    // Position X of the exit
    std::uniform_int_distribution<int> uniform_int_x_distribution(0, width_ - 2);// -2 because we do a +1 in gl_maze
    int x_position = uniform_int_x_distribution(mersenne_twister_);

    // Position Z of the exit
    std::uniform_int_distribution<int> uniform_int_z_distribution(0, height_ - 2);// -2 because we do a +1 in gl_maze
    int z_position = uniform_int_z_distribution(mersenne_twister_);

    if (wall_position == 0) {
        grid_[0][x_position].setFrontier(Cell::Direction::N, false);
        exit_position_ = std::pair<int, int>(x_position, 0);
    } else if (wall_position == 1) {
        grid_[z_position][width_ - 1].setFrontier(Cell::Direction::E, false);
        exit_position_ = std::pair<int, int>(width_ - 1, z_position);
    } else if (wall_position == 2) {
        grid_[height_ - 1][x_position].setFrontier(Cell::Direction::S, false);
        exit_position_ = std::pair<int, int>(x_position, height_ - 1);
    } else if (wall_position == 3) {
        grid_[z_position][0].setFrontier(Cell::Direction::W, false);
        exit_position_ = std::pair<int, int>(0, z_position);
    }
}

void Maze::generateUserAndSpherePosition() {
    // Start at 1 to stay inside when centering
    std::uniform_int_distribution<int> uniform_int_x_distribution(1, this->width_ - 1);
    std::uniform_int_distribution<int> uniform_int_z_distribution(1, this->height_ - 1);

    this->user_position_.first = uniform_int_x_distribution(mersenne_twister_);
    this->sphere_position_.first = uniform_int_x_distribution(mersenne_twister_);

    this->user_position_.second = uniform_int_z_distribution(mersenne_twister_);
    this->sphere_position_.second = uniform_int_z_distribution(mersenne_twister_);
}

const pair<int, int> &Maze::getSpherePosition() const {
    return sphere_position_;
}

const pair<int, int> &Maze::getUserPosition() const {
    return user_position_;
}

const pair<int, int> &Maze::getExitPosition() const {
    return exit_position_;
}
