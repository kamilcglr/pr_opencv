#include <QtGui/QPainter>
#include <iostream>
#include <QtWidgets/QGraphicsOpacityEffect>
#include <QtCore/QPropertyAnimation>
#include "top_view.h"

TopView::TopView(QWidget *parent) : QWidget(parent) {


}

void TopView::paintEvent(QPaintEvent *) {
    QPainter painter(this);
    painter.eraseRect(0, 0, width(), height());
    painter.setWindow(0, 0, this->maze_->getWidth(), this->maze_->getHeight());
    painter.setViewport(10, 10, width() - 20, height() - 20);
    painter.setPen(QPen(QColor(qRgb(255, 255, 255)), 0.02, Qt::SolidLine));

    // Print the first line
    for (int x = 0; x < this->maze_->getWidth(); x++) {
        if (this->maze_->get_grid()[0][x].isFrontier(Cell::N)) {
            painter.drawLine(x, 0, x + 1, 0);
        }
    }

    // Print other lines
    for (int z = 0; z < this->maze_->getHeight(); z++) {

        // Beginning of line
        if (this->maze_->get_grid()[z][0].isFrontier(Cell::W)) {
            painter.drawLine(0, z, 0, z + 1);
        }

        for (int x = 0; x < this->maze_->getWidth(); x++) {
            if (this->maze_->get_grid()[z][x].isFrontier(Cell::E)) {
                painter.drawLine(x + 1, z, x + 1, z + 1);
            }
        }

        // Print horizontal frontier
        for (int j = 0; j < this->maze_->getWidth(); j++) {
            if (this->maze_->get_grid()[z][j].isFrontier(Cell::S)) {
                painter.drawLine(j, z + 1, j + 1, z + 1);
            }
        }
    }
    painter.setPen(QPen(QColor(qRgb(3, 218, 198)), 0.1, Qt::SolidLine));
    painter.drawEllipse(position_camera_, 0.1, 0.1);
    painter.drawLine(position_camera_, target_camera_);
}

void TopView::setMaze(Maze *maze) {
    this->maze_ = maze;
}

void TopView::setNewPositionAndTarget(QPointF position_camera, QPointF target_camera) {
    this->position_camera_ = position_camera;
    this->target_camera_ = target_camera;
    this->repaint();
}
