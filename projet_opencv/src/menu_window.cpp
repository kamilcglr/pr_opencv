#include "menu_window.h"
#include "ui_menu_window.h"
#include "game/game_window.h"
#include <QtGui>
#include <QDialog>
#include <iostream>
#include "options_dialog.h"

using namespace std;

MenuWindow::MenuWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MenuWindow) {
    ui->setupUi(this);
    initiateGIF();
    dialog_ = new OptionsDialog();
    connect(dialog_, SIGNAL(accepted()), this, SLOT(getOptions()));
}

MenuWindow::~MenuWindow() {
    delete ui;
}

void MenuWindow::on_playButton_clicked() {
    game_window = new GameWindow(options.first, options.second);
    game_window->show();
    this->setVisible(false);
    connect(game_window, &GameWindow::enablePlayButton, [&] {
        this->setVisible(true);
    });
}

void MenuWindow::on_optionsButton_clicked() {
    dialog_->show();
}

void MenuWindow::on_quitButton_clicked() {
    exit(0);
}

void MenuWindow::getOptions() {
    options = dialog_->getOptions();
}

void MenuWindow::initiateGIF() {
    ui->gif_label->setMovie(new QMovie("res/pictures/maze.gif"));
    ui->gif_label->movie()->start();
}
